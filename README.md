# 🎲 geminoe

Static site generator for outputting Gemini & HTML.

- Internationalisation
- Templating engine
- Gemtext & HTML output
- Auto-refresh with `chokidar` & `browser-sync`

## Example

`~content/posts/en/2021-01-01-blog-post.md`

```markdown
+++
parent="post.md"
title="Example"
date=2021-01-01T15:13:56Z
+++

An example of what's possible with **geminoe**!.
```

`~templates/post.md`

```html
+++
parent="root.html"
+++

# {{ site::title }} ## {{ child::data.title }} {{ child::data.date | date }} `{{ child::content | md5 }}`
{{child::content | markdown }}
```

## Development

```
npm run start -- --source=/Users/cass/Code/Sites/cass.si
```
