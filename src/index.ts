#!/usr/bin/env node

import toml from "toml";
import pino from "pino";
import yargs, { number } from "yargs";

const { hideBin } = require("yargs/helpers");
import matter, { GrayMatterFile } from "gray-matter";
import marked from "marked";
import chokidar from "chokidar";
import md5 from "md5";
import { renderSync } from "node-sass";

import { resolve } from "path";
import fs from "fs";
import { readFile, readdir, mkdir, rmdir } from "fs/promises";
import { writeFile } from "fs-path";
import browserSync from "browser-sync";
import { IConfig, IFMCache, ITemplate } from "./interfaces";
import { Ora } from "ora";
const ora = require("ora");

const Formats = ["text/html", "text/gemini"] as const;
type Format = typeof Formats[number];

const FormatExtension: { [index in Format]: string } = { "text/gemini": "gmi", "text/html": "html" };

type PipeData = {
  acc: string;
  args: string[];
  child: GrayMatterFile<string>;
  locale: string;
};

const operands = ["iterate", "i18n", "site", "child", "page", "include", "html", "gemini"] as const;
const pipes = ["i18n", "date", "uppercase", "lowercase", "markdown", "md5"] as const;

export class Geminoe {
  pipes: { [index in typeof pipes[number]]: (data: PipeData) => Promise<string> } = {
    i18n: async (data) => this.config.languages[this.currentLocale][data.acc],
    date: async (data) => new Date(data.acc).toLocaleDateString(),
    uppercase: async (data) => data.acc.toUpperCase(),
    lowercase: async (data) => data.acc.toLowerCase(),
    markdown: async (data) => marked(data.acc),
    md5: async (data) => md5(data.acc),
  };

  operandMap: { [index in typeof operands[number]]?: (data: PipeData) => Promise<string> } = {
    site: async (data) => {
      return this.config[data.args[0]];
    },
    child: async (data) => {
      const positionals = data.args[0].split(".");
      if (positionals[0] == "content") return data.child.content;
      if (positionals[0] == "data") return data.child.data[positionals[1]];
    },
    iterate: async (data) => {
      const [key, include] = data.args[0].split(" ");
      const files = Object.keys(this.cache).filter((k) => k.includes(key));

      // Parse each include
      const html = await Promise.all(
        files.map((f) => {
          return this.parse(`${this.sourcePath}/~includes/${include}`, f, this.currentLocale, {
            content: "",
            data: this.cache[f],
          } as any).then((v) => {
            v.body.frontmatter = this.cache[f];
            return v;
          });
        })
      );

      // Sort by date, filter out drafts & add on iterated body onto content
      return html
        .sort((a, b) => (a.body.frontmatter["date"] < b.body.frontmatter["date"] ? 1 : -1))
        .filter((v) => !v.body.frontmatter["draft"])
        .reduce((acc, curr) => ((acc += `${curr.body.content}`), acc), "");
    },
    include: async (data) => {
      const include = data.args[0];
      const html = await this.parse(`${this.sourcePath}/~includes/${include}`, "", data.locale, null);

      return html.body.content;
    },
    i18n: async (data) => {
      if (data.args[0][0] == "(" && data.args[0][data.args[0].length - 1] == ")") {
        const json = JSON.parse(`{${data.args[0].substring(1, data.args[0].length - 1)}}`);
        if (!json[data.locale]) this.log.error(`Could not find locale ${data.locale} in i18n map: ${data.args}`);
        return json[data.locale];
      }

      return this.config.languages[data.locale][data.args[0]];
    },
    html: async (data) => (console.log("--->", data), this.format == "text/html" ? data.args.join("") : ""),
    gemini: async (data) => (this.format == "text/gemini" ? data.acc + data.args.join("") : data.acc),
  };

  spinner: Ora;
  currentLocale: string;
  isPrimaryLocale: boolean; // current locale generation is the primary locale
  startTime: Date;
  generatedPageCount: number;
  sourcePath: string; // input location
  destinationPath: string; // build location
  log: pino.Logger;
  config: IConfig; // config.toml file
  cache: IFMCache; // running list of all compiled pages frontmatter
  format: Format;

  constructor(format: Format, source: string = process.cwd(), dest: string = "dist") {
    this.sourcePath = source;
    this.format = format;
    this.spinner = ora();
    this.destinationPath = `${this.sourcePath}/${dest}/${FormatExtension[this.format]}`;

    this.log = pino({
      level: "debug",
      prettyPrint: {
        colorize: true,
        messageFormat: "\x1B[37m{msg}",
        levelFirst: true,
        ignore: "hostname,pid,time",
      },
    });
  }

  serve(port: number = 4200) {
    const web = browserSync.init({
      port: port,
      open: false,
      server: {
        baseDir: this.destinationPath,
        serveStaticOptions: {
          extensions: ["html"],
        },
      },
    });

    const watcher = chokidar.watch(this.sourcePath, {
      ignoreInitial: true,
      ignored: (path) => ["node_modules", this.destinationPath].some((s) => path.includes(s)),
    });

    watcher.on("ready", async () => {
      await this.build();

      watcher.on("change", async (event, path) => {
        await this.build();
        web.reload();
      });
    });
  }

  async build() {
    this.log.info(`geminoe ${process.env.npm_package_version}`);
    this.startTime = new Date();
    this.isPrimaryLocale = false;
    this.generatedPageCount = 0;
    this.cache = {};

    this.config = toml.parse(await readFile(`${this.sourcePath}/config.toml`, "utf8"));
    this.config.time = new Date();

    fs.existsSync(this.destinationPath) && (await rmdir(this.destinationPath, { recursive: true }));
    !fs.existsSync(this.destinationPath) && (await mkdir(this.destinationPath, { recursive: true }));

    // Generate all pages for each locale
    for await (const locale of Object.keys(this.config.languages)) {
      this.log.info(`Generating ${this.format} for locale ${locale}...`);
      this.spinner.start();
      this.isPrimaryLocale = locale == this.config.primary_locale;

      if (!this.isPrimaryLocale) await mkdir(`${this.destinationPath}/${locale}`);

      // Recursive directory generator
      const traverse = async (dir) => {
        const dirents = await readdir(dir, { withFileTypes: true });
        const files = await Promise.all(
          dirents.map((dirent) => {
            const res = resolve(dir, dirent.name);
            return dirent.isDirectory() ? traverse(res) : res;
          })
        );
        return Array.prototype.concat(...files);
      };

      // Parse deepest files first, building up the cache as we go
      const files = (await traverse(`${this.sourcePath}/~content/.`)).sort((a, b) => {
        return a.split("/").length > b.split("/").length ? -1 : 1;
      });

      // Traverse the site content tree, generating each page as the final .html and writing it to the locale directory
      for await (let file of files) {
        if (["html", "md"].includes(file.split(".").pop())) {
          let basePath = file.replace(`${this.sourcePath}/~content/`, "");

          // See if current file contains one of the locales
          const isI18n = basePath
            .split("/")
            .filter((value: string) => Object.keys(this.config.languages).includes(value));

          // Only parse files in a locale folder for this current locale
          if (isI18n.length && isI18n[0] !== locale) continue;

          // Strip out /locale/ from path - to be prepended onto final path e.g. /locale/filepath
          if (isI18n && isI18n[0] == locale) basePath = basePath.replace(`/${locale}/`, "/");

          this.spinner.text = `\tParsing ${file.split("/").pop()}...\n`;

          // Get the destination path of the file, generate it & save it
          const fileDest = `${this.isPrimaryLocale ? "" : locale + "/"}${basePath.replace(/\.[^.]+$/, ".html")}`;
          const body = await this.parse(file, fileDest, locale).finally(() => this.generatedPageCount++);

          await new Promise((res) => writeFile(`${this.destinationPath}/${fileDest}`, body.body.content, res));
        }
      }

      this.spinner.text = "";
      this.spinner.stop();
    }

    // HTML specific stuff, like generating stylesheets
    if (this.format == "text/html") {
      // Parse the stylesheets into CSS from SCSS
      await new Promise(async (res) =>
        writeFile(
          `${this.destinationPath}/styles.css`,
          (
            await Promise.all(
              (
                await readdir(`${this.sourcePath}/~styles/`)
              ).map((f) => readFile(`${this.sourcePath}/~styles/${f}`, "utf-8"))
            )
          )
            .map((f) => renderSync({ data: f }).css)
            .reduce((acc, curr) => ((acc += `${curr}`), acc), ""),
          res
        )
      );
    }

    this.log.info(
      `FINISHED: generated ${this.generatedPageCount} files in ${new Date().getTime() - this.startTime.getTime()}ms`
    );
  }

  async parse(
    sourcePath: string,
    destinationPath: string,
    locale: string,
    child?: GrayMatterFile<string>
  ): Promise<ITemplate> {
    // Extract all template data, frontmatter & body
    const node = matter(await readFile(sourcePath, "utf8"), {
      delimiters: "+++",
      language: "toml",
      engines: {
        toml: toml.parse.bind(toml),
      },
    });

    // Keep a running list of all previously compiled templates frontmatter
    node.data["url"] = destinationPath;
    this.cache[sourcePath.replace(this.sourcePath, "")] = node.data;

    // FIX: hack in current locale in place of $locale on each page
    node.content = node.content.replaceAll("$locale", locale);
    node.content = node.content.replaceAll("!locale", this.isPrimaryLocale ? "" : `/${locale}`);

    // Parse all {{ tags }}, including multi-line ones by char by char, looking for start & end delimiters
    const tags = (() => {
      let i = 0;
      const tags = [];
      let start = null;
      let end = null;
      while (i <= node.content.length) {
        start = start !== null ? start : node.content.substring(i, i + 2) == "{{" ? i : null;
        end = end !== null ? end : node.content.substring(i, i + 2) == "}}" ? i + 2 : null;

        if (start && end) {
          tags.push(node.content.substring(start, end));
          start = null;
          end = null;
        }

        i++;
      }

      return tags;
    })();

    const mapping = {};
    for await (const tag of tags || []) {
      mapping[tag] = await tag
        .replaceAll(/({{|}})/g, "")
        .trim()
        .split("|")
        .map((v: string) => v.trim())
        .reduce(async (acc, curr) => {
          let pipe = this.pipes[curr];
          let args: string[];

          if (!pipe) {
            if (curr.includes("::")) {
              const op = curr.split("::").map(
                (t: string) =>
                  t
                    .replaceAll("\n", "") // all newline chars
                    .replace(/\s{2,}/g, "") // all additional spaces
                    .trim() // all left/right space chars
              );
              pipe = this.operandMap[op[0]];
              args = (op.shift(), op);
            }
          }

          if (!pipe) this.log.error(`Unknown pipe: ${curr}`);
          return await pipe({ acc: await acc, args, child, locale });
        }, Promise.resolve(node.content));
    }

    // Replace all tags with mapped value
    Object.keys(mapping).forEach((tag) => {
      node.content = node.content.replaceAll(tag, mapping[tag]);
    });

    return {
      body: {
        content: node.data.parent
          ? (await this.parse(`${this.sourcePath}/~templates/${node.data.parent}`, destinationPath, locale, node)).body
              .content
          : node.content,
        frontmatter: node.data,
      },
    };
  }
}

yargs(hideBin(process.argv))
  .command(
    "serve [port]",
    "start the html server",
    (yargs) => {
      yargs.positional("port", {
        describe: "port to bind on",
        default: 4200,
      });
    },
    (argv) => {
      if (argv.verbose) console.info(`start server on :${argv.port}`);
      const i = new Geminoe("text/html", argv.source as string);
      i.serve(parseInt(argv.port as string));
    }
  )
  .command(
    "build",
    "build contents",
    (yargs) => {
      yargs.positional("format", {
        describe: "output format, 'text/gemini' or 'text/html'",
        default: "text/html",
      });
      yargs.positional("source", {
        describe: "source path",
        default: "",
      });
    },
    (argv) => {
      if (!Formats.includes(argv["format"] as any)) throw new Error("Not a valid format!");
      new Geminoe(argv["format"] as Format).build();
    }
  )
  .option("source", {
    alias: "s",
    type: "string",
  })
  .option("verbose", {
    alias: "v",
    type: "boolean",
    description: "Run with verbose logging",
  }).argv;
