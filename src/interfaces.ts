import { GrayMatterFile } from "gray-matter";

export interface IConfig {
  title: string;
  url: string;
  time: Date;
  primary_locale: string;
  languages: {
    [locale: string]: {
      [key: string]: string;
    };
  };

  [index: string]: any;
}

export type IFMCache = { [index: string]: GrayMatterFile<string>["data"] };

export interface ITemplate {
  body: {
    content: string;
    frontmatter: { [index: string]: any };
  };
}